<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInita52979f817e6744b7f85f4422f10ed93
{
    public static $prefixLengthsPsr4 = array (
        'C' => 
        array (
            'Campbest\\RestngFilter\\' => 22,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Campbest\\RestngFilter\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInita52979f817e6744b7f85f4422f10ed93::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInita52979f817e6744b7f85f4422f10ed93::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInita52979f817e6744b7f85f4422f10ed93::$classMap;

        }, null, ClassLoader::class);
    }
}
