<?php

// autoload_real.php @generated by Composer

class ComposerAutoloaderInita52979f817e6744b7f85f4422f10ed93
{
    private static $loader;

    public static function loadClassLoader($class)
    {
        if ('Composer\Autoload\ClassLoader' === $class) {
            require __DIR__ . '/ClassLoader.php';
        }
    }

    /**
     * @return \Composer\Autoload\ClassLoader
     */
    public static function getLoader()
    {
        if (null !== self::$loader) {
            return self::$loader;
        }

        spl_autoload_register(array('ComposerAutoloaderInita52979f817e6744b7f85f4422f10ed93', 'loadClassLoader'), true, true);
        self::$loader = $loader = new \Composer\Autoload\ClassLoader(\dirname(__DIR__));
        spl_autoload_unregister(array('ComposerAutoloaderInita52979f817e6744b7f85f4422f10ed93', 'loadClassLoader'));

        require __DIR__ . '/autoload_static.php';
        \Composer\Autoload\ComposerStaticInita52979f817e6744b7f85f4422f10ed93::getInitializer($loader)();

        $loader->register(true);

        return $loader;
    }
}
