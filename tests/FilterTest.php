<?php

namespace Tests;

use Carbon\Carbon;
use Campbest\RestngFilter\Filter;
use PHPUnit\Framework\TestCase;

class FilterTest extends TestCase
{

	private function buildData() {
		$rec1 = array('uid'=>'campbest','record'=>array('a'=>'b','b'=>'c'),'test'=>123); 
		$rec2 = array('uid'=>'jonesa1','record'=>array('a'=>'b','b'=>'c'),'test'=>123); 
		$rec3 = array('uid'=>'peters1','record'=>array('a'=>'b','b'=>'c'),'test'=>123); 
		$data = array($rec1,$rec2,$rec3);
		return $data;
	}

	public function testReturnsJSON(): void
	{
		$data=array('a'=>'b');
		$f = new Filter();
		$r = $f->filter($data);

		$this->assertEquals($data,$r);
	}

	public function handlesInvalidView():void 
	{}

	public function handlesInvalidAccount(): void
	{}

	public function testFiltersValidPresent():void
	{
		$data = $this->buildData();
		$f = new Filter("gpa","campbest");
		$r = $f->filter($data);
		//make sure campbest is present
		$ok=false;
		foreach ($r as $i) {
			if ($i['uid'] == "campbest")
				$ok=true;
		}
		$this->assertTrue($ok);
	}

	public function testFiltersNoNonValidPresent():void
	{

		$data = $this->buildData();
		$f = new Filter("gpa","campbest");
		$r = $f->filter($data);
		//make sure No others are present
		foreach ($r as $i) {
			if ($i['uid'] != "campbest")
				$this->fail("invalid uid in response");
		}
	}





}
